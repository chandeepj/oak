# Oak Framework: A Wordpress Theme Framework

Oak Framework is a composer package that simplifies some of the complexities of theme development and helps you write more modern PHP code.

## Requirements

* WordPress 4.9.6+.
* PHP 5.6+ (7.0+ recommended).
* [Composer](https://getcomposer.org/) for managing PHP dependencies.

## Copyright and License

Oak Framework is licensed under the [GNU GPL](https://www.gnu.org/licenses/gpl-2.0.html), version 2 or later.

2019 &copy; Chandeep J.
