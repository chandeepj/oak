<?php
/**
 * General template functions.  These functions are for use throughout the theme's various template files.
 * Their main purpose is to handle many of the template tags that are currently lacking in core WordPress.
 *
 * @package Oak
 */

namespace Oak\Site;

/**
 * Outputs the link back to the site.
 *
 * @param  string 	(optional) link class name
 * @access public
 * @return void
 */
function display_link( $class = 'site-link' ) {

	echo render_link( $class );
}

/**
 * Returns a link back to the site.
 *
 * @param  string 	(optional) link class name
 * @access public
 * @return string
 */
function render_link( $class = 'site-link' ) {

	return sprintf( 
		'<a class="%s" href="%s" rel="home">%s</a>',
		$class, 
		esc_url( home_url() ), 
		get_bloginfo( 'name' ) 
	);
}

/**
 * Displays a link to WordPress.org.
 *
 * @param  string 	(optional) link class name
 * @access public
 * @return void
 */
function display_wp_link( $class = 'wp-link' ) {

	echo render_wp_link( $class );
}

/**
 * Returns a link to WordPress.org.
 * 
 * @param  string 	(optional) link class name
 * @access public
 * @return string
 */
function render_wp_link( $class = 'wp-link' ) {

	return sprintf( 
		'<a class="%s" href="%s" rel="nofollow">%s</a>', 
		$class,
		esc_url( __( 'https://wordpress.org', 'oak-framework' ) ), 
		esc_html__( 'WordPress', 'oak-framework' ) 
	);
}

/**
 * Displays a link to the parent theme URI.
 *
 * @param  string 	(optional) link class name
 * @access public
 * @return void
 */
function display_theme_link( $class = 'theme-link' ) {

	echo render_theme_link( $class );
}

/**
 * Returns a link to the parent theme URI.
 *
 * @param  string 	(optional) link class name
 * @access public
 * @return string
 */
function render_theme_link( $class = 'theme-link' ) {
	$theme   = wp_get_theme( get_template() );
	$allowed = [ 
		'abbr' => [ 'title' => true ], 
		'acronym' => [ 'title' => true ], 
		'code' => true, 
		'em' => true, 
		'strong' => true 
	];

	// Note: URI is escaped via `WP_Theme::markup_header()`.
	return sprintf( 
		'<a class="%s" href="%s" rel="nofollow">%s</a>', 
		$class,
		$theme->display( 'ThemeURI' ), 
		wp_kses( $theme->display( 'Name' ), $allowed ) 
	);
}

/**
 * Displays a link to the child theme URI.
 *
 * @param  string 	(optional) link class name
 * @access public
 * @return void
 */
function display_child_theme_link( $class = 'child-link' ) {

	echo render_child_theme_link( $class );
}

/**
 * Returns a link to the child theme URI.
 *
 * @param  string 	(optional) link class name
 * @access public
 * @return string
 */
function render_child_theme_link( $class = 'child-link' ) {

	if ( ! is_child_theme() )
		return '';

	$theme   = wp_get_theme();
	$allowed = [ 
		'abbr' => [ 'title' => true ], 
		'acronym' => [ 'title' => true ], 
		'code' => true, 
		'em' => true, 
		'strong' => true 
	];

	// Note: URI is escaped via `WP_Theme::markup_header()`.
	return sprintf( 
		'<a class="%s" href="%s" rel="nofollow">%s</a>', 
		$class,
		$theme->display( 'ThemeURI' ), 
		wp_kses( $theme->display( 'Name' ), $allowed ) 
	);
}

/**
 * Gets the "blog" (posts page) page URL.  `home_url()` will not always work for this because it
 * returns the front page URL.  Sometimes the blog page URL is set to a different page.  This
 * function handles both scenarios.
 *
 * @access public
 * @return string
 */
function render_blog_url() {

	if ( 'posts' === get_option( 'show_on_front' ) )
		$blog_url = home_url();

	elseif ( 0 < ( $page_for_posts = get_option( 'page_for_posts' ) ) )
		$blog_url = get_permalink( $page_for_posts );

	return ! empty( $blog_url ) ? esc_url( $blog_url ) : '';
}

/**
 * Outputs the site title.
 *
 * @param  string 	(optional) class prefix for bem
 * @access public
 * @return void
 */
function display_title( $class_prefix = 'c-branding' ) {

	echo render_title( $class_prefix );
}

/**
 * Returns the linked site title wrapped in an `<h1>` tag on home
 *
 * @param  string 	(optional) class prefix for bem
 * @access public
 * @return string
 */
function render_title( $class_prefix = 'c-branding' ) {

	if ( $title = get_bloginfo( 'name' ) ) {
		if ( is_plural() ) {
			$title = sprintf( 
				'<h1 class="%1$s__title">
					<a class="%1$s__title-link" href="%2$s" rel="home">%3$s</a>
				</h1>', 
				$class_prefix,
				esc_url( home_url() ), 
				$title 
			);
		} else {
			$title = sprintf( 
				'<h2 class="%1$s__title">
					<a class="%1$s__title-link" href="%2$s" rel="home">%3$s</a>
				</h2>', 
				$class_prefix,
				esc_url( home_url() ), 
				$title 
			);
		}
	}

	return apply_filters( 'oak/site/title', $title );
}

/**
 * Outputs the site description.
 *
 * @param  string 	(optional) class prefix for bem
 * @access public
 * @return void
 */
function display_description( $class_prefix = 'c-branding' ) {

	echo render_description( $class_prefix );
}

/**
 * Returns the site description wrapped in an `<p>` tag.
 *
 * @param  string 	(optional) class prefix for bem
 * @access public
 * @return string
 */
function render_description( $class_prefix = 'c-branding' ) {

	if ( $desc = get_bloginfo( 'description' ) )
		$desc = sprintf( '<p class="%s__description">%s</p>', $class_prefix, $desc );

	return apply_filters( 'oak/site/description', $desc );
}

/**
 * Function for figuring out if we're viewing a "plural" page.  In WP, these pages are archives,
 * search results, and the home/blog posts index.  Note that this is similar to, but not quite
 * the same as `!is_singular()`, which wouldn't account for the 404 page.
 *
 * @access public
 * @return bool
 */
function is_plural() {

	return apply_filters( 'oak/is/plural', is_home() || is_archive() || is_search() );
}

/**
 * Print the general archive title.
 *
 * @access public
 * @return void
 */
function display_single_archive_title() {

	echo render_single_archive_title();
}

/**
 * Retrieve the general archive title.
 *
 * @access public
 * @return string
 */
function render_single_archive_title() {

	return esc_html__( 'Archives', 'oak-framework' );
}

/**
 * Print the author archive title.
 *
 * @access public
 * @return void
 */
function display_single_author_title() {

	echo render_single_author_title();
}

/**
 * Retrieve the author archive title.
 *
 * @access public
 * @return void
 */
function render_single_author_title() {

	return get_the_author_meta( 'display_name', absint( get_query_var( 'author' ) ) );
}

/**
 * Print the year archive title.
 *
 * @access public
 * @return void
 */
function display_single_year_title() {

	echo render_single_year_title();
}

/**
 * Retrieve the year archive title.
 *
 * @access public
 * @return string
 */
function render_single_year_title() {

	return get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'oak-framework' ) );
}

/**
 * Print the week archive title.
 *
 * @access public
 * @return void
 */
function display_single_week_title() {

	echo render_single_week_title();
}

/**
 * Retrieve the week archive title.
 *
 * @access public
 * @return string
 */
function render_single_week_title() {

	// Translators: 1 is the week number and 2 is the year.
	return sprintf( 
		esc_html__( 'Week %1$s of %2$s', 'oak-framework' ), 
		get_the_time( esc_html_x( 'W', 'weekly archives date format', 'oak-framework' ) ), 
		get_the_time( esc_html_x( 'Y', 'yearly archives date format', 'oak-framework' ) ) 
	);
}

/**
 * Print the day archive title.
 *
 * @access public
 * @return void
 */
function display_single_day_title() {

	echo render_single_day_title();
}

/**
 * Retrieve the day archive title.
 *
 * @access public
 * @return string
 */
function render_single_day_title() {

	return get_the_date( esc_html_x( 'F j, Y', 'daily archives date format', 'oak-framework' ) );
}

/**
 * Print the hour archive title.
 *
 * @access public
 * @return void
 */
function display_single_hour_title() {

	echo render_single_hour_title();
}

/**
 * Retrieve the hour archive title.
 *
 * @access public
 * @return string
 */
function render_single_hour_title() {

	return get_the_time( esc_html_x( 'g a', 'hour archives time format', 'oak-framework' ) );
}

/**
 * Print the minute archive title.
 *
 * @access public
 * @return void
 */
function display_single_minute_title() {

	echo render_single_minute_title();
}

/**
 * Retrieve the minute archive title.
 *
 * @access public
 * @return string
 */
function render_single_minute_title() {

	// Translators: Minute archive title. %s is the minute time format.
	return sprintf( 
		esc_html__( 'Minute %s', 'oak-framework' ), 
		get_the_time( esc_html_x( 'i', 'minute archives time format', 'oak-framework' ) ) 
	);
}

/**
 * Print the minute + hour archive title.
 *
 * @access public
 * @return void
 */
function display_single_minute_hour_title() {

	echo render_single_minute_hour_title();
}

/**
 * Retrieve the minute + hour archive title.
 *
 * @access public
 * @return string
 */
function render_single_minute_hour_title() {

	return get_the_time( esc_html_x( 'g:i a', 'minute and hour archives time format', 'oak-framework' ) );
}

/**
 * Print the search results title.
 *
 * @access public
 * @return void
 */
function display_search_title() {

	echo render_search_title();
}

/**
 * Retrieve the search results title.
 *
 * @access public
 * @return string
 */
function render_search_title() {

	// Translators: %s is the search query.
	return sprintf( 
		esc_html__( 'Search results for: %s', 'oak-framework' ), 
		get_search_query() 
	);
}

/**
 * Show Numbered Page Nav Links. This function is to warp the core wordpress paginate_links() function
 * with div.paging and ul tag, along with changing the default args as needed.
 *
 * @access public
 * @return void
 */
function display_paginate_links( $args = [] ) {
	$defaults = [
		'mid_size'  => 2,
		'prev_text' => sprintf(
			'<span class="nav-prev-text">%s</span>',
			__('&laquo; Prev', 'oak-framework')
		),
		'next_text' => sprintf(
			'<span class="nav-next-text">%s</span>',
			__('Next &raquo;', 'oak-framework')
		),
	];
	$args = wp_parse_args( $args, $defaults );
	
	the_posts_pagination( $args );
}