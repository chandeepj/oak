<?php
/**
 * Framwork bootstrap process.
 *
 * This file bootstraps parts of the framework that can't be autoloaded. We
 * define any global constants here and load any additional functions files.
 *
 * @package   Oak
 * @version   0.3.3
 * @author    Chandeep <chandeep@dezinewoods.com>
 * @copyright Copyright (c) 2018 - 2019, Chandeep
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

# Define the directory path to the framework. This shouldn't need changing
# unless doing something really out there or just for clarity.
if ( ! defined( 'OAK_DIR' ) ) {
	define( 'OAK_DIR', __DIR__ );
}

# Check if the framework has been bootstrapped. If not, load the bootstrap files
# and get the framework set up.
if ( ! defined( 'OAK_BOOTSTRAPPED' ) ) {

    require_once( 'bootstrap-functions.php' );

	define( 'OAK_BOOTSTRAPPED', true );
}