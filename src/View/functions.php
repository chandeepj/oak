<?php
/**
 * View template tags.
 *
 * Template functions related to views.
 *
 * @package   Oak
 */

namespace Oak\View;

use Oak\Tools\Collection;

/**
 * Returns a view object.
 *
 * @access public
 * @param  string            $name
 * @param  array|string      $slugs
 * @param  array|Collection  $data
 * @return View
 */
function view( $name, $slugs = [], $data = [] ) {

	if ( ! $data instanceof Collection ) {
		$data = new Collection( $data );
	}

	return new View( $name, $slugs, $data );
}

/**
 * Outputs a view template.
 *
 * @access public
 * @param  string            $name
 * @param  array|string      $slugs
 * @param  array|Collection  $data
 * @return void
 */
function display( $name, $slugs = [], $data = [] ) {

	view( $name, $slugs, $data )->display();
}

/**
 * Returns a view template as a string.
 *
 * @access public
 * @param  string            $name
 * @param  array|string      $slugs
 * @param  array|Collection  $data
 * @return string
 */
function render( $name, $slugs = [], $data = [] ) {

	return view( $name, $slugs, $data )->render();
}
