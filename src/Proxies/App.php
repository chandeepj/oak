<?php
/**
 * App static proxy class.
 *
 * Static proxy class for the application instance.
 *
 * @package   Oak
 */

namespace Oak\Proxies;

/**
 * App static proxy class.
 *
 * @access public
 */
class App extends Proxy {

	/**
	 * Returns the name of the accessor for object registered in the container.
	 *
	 * @access protected
	 * @return string
	 */
	protected static function accessor() {

		return 'app';
	}
}
