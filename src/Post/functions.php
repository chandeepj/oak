<?php
/**
 * Entry functions.
 *
 * Helper functions and template tags related to posts.
 * 
 * @package Oak
 */

namespace Oak\Post;

/**
 * Creates a hierarchy based on the current post. Its primary purpose is for
 * use with post views/templates.
 *
 * @access public
 * @return array
 */
function hierarchy() {

	// Set up an empty array and get the post type.
	$hierarchy = [];
	$post_type = get_post_type();

	// If attachment, add attachment type templates.
	if ( 'attachment' === $post_type ) {

		extract( get_mime_types() );

		if ( $subtype ) {
			$hierarchy[] = "attachment-{$type}-{$subtype}";
			$hierarchy[] = "attachment-{$subtype}";
		}

		$hierarchy[] = "attachment-{$type}";
	}

	// If the post type supports 'post-formats', get the template based on the format.
	if ( post_type_supports( $post_type, 'post-formats' ) ) {

		// Get the post format.
		$post_format = get_post_format() ?: 'standard';

		// Template based off post type and post format.
		$hierarchy[] = "{$post_type}-{$post_format}";

		// Template based off the post format.
		$hierarchy[] = $post_format;
	}

	// Template based off the post type.
	$hierarchy[] = $post_type;

	return apply_filters( 'oak/post/hierarchy', $hierarchy );
}

/**
 * Outputs the post title HTML.
 *
 * @access public
 * @param  array  $args
 * @return void
 */
function display_title( array $args = [] ) {

	echo render_title( $args );
}

/**
 * Returns the post title HTML.
 *
 * @access public
 * @param  array  $args
 * @return string
 */
function render_title( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'post_id' 		=> get_the_ID(),
		'text'   		=> '%s',
		'tag'    		=> is_singular() ? 'h1' : 'h2',
		'link'   		=> ! is_singular(),
		'class'  		=> 'l-entry__title',
		'link_class'  	=> 'l-entry__permalink',
		'before' 		=> '',
		'after'  		=> ''
	] );

	$class = ( $args['class'] !== '' ) ? 'class="'.esc_attr( $args['class'] ).'"' : '';
	$text = sprintf( $args['text'], get_the_title( $args['post_id'] ) );

	if ( $args['link'] ) {
		$text = render_permalink( [ 'post_id' => $args['post_id'], 'text' => $text, 'class' => $args['link_class'] ] );
	}

	$html = '';
	if ( $text ) {
		$html = sprintf(
			'<%1$s %2$s>%3$s</%1$s>',
			tag_escape( $args['tag'] ),
			$class,
			$text
		);
	}

	return apply_filters( 'oak/post/title', $args['before'] . $html . $args['after'] );
}


/**
 * Outputs the post permalink HTML.
 *
 * @access public
 * @param  array  $args
 * @return void
 */
function display_permalink( array $args = [] ) {

	echo render_permalink( $args );
}

/**
 * Returns the post permalink HTML.
 *
 * @access public
 * @param  array  $args
 * @return string
 */
function render_permalink( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'post_id' => get_the_ID(),
		'text'    => '%s',
		'class'   => 'l-entry__permalink',
		'before'  => '',
		'after'   => ''
	] );
	$class = ( $args['class'] !== '' ) ? 'class="'.esc_attr( $args['class'] ).'"' : '';
	$url = get_permalink( $args['post_id'] );

	$html = sprintf(
		'<a %s href="%s">%s</a>',
		$class,
		esc_url( $url ),
		sprintf( $args['text'], esc_url( $url ) )
	);

	return apply_filters( 'oak/post/permalink', $args['before'] . $html . $args['after'] );
}

/**
 * Outputs the post author HTML.
 *
 * @access public
 * @param  array  $args
 * @return void
 */
function display_author( array $args = [] ) {

	echo render_author( $args );
}

/**
 * Returns the post author HTML.
 *
 * @access public
 * @param  array  $args
 * @return string
 */
function render_author( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'text'   => '%s',
		'class'  => 'l-entry__author',
		'before' => '',
		'after'  => ''
	] );

	$url = get_author_posts_url( get_the_author_meta( 'ID' ) );

	$html = sprintf(
		'<a class="%s" href="%s">%s</a>',
		esc_attr( $args['class'] ),
		esc_url( $url ),
		sprintf( $args['text'], get_the_author() )
	);

	return apply_filters(
		'oak/post/author',
		$args['before'] . $html . $args['after']
	);
}

/**
 * Outputs the post date HTML.
 *
 * @access public
 * @param  array  $args
 * @return void
 */
function display_date( array $args = [] ) {

	echo render_date( $args );
}

/**
 * Returns the post date HTML.
 *
 * @access public
 * @param  array  $args
 * @return string
 */
function render_date( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'text'   => '%s',
		'class'  => 'l-entry__published',
		'format' => '',
		'before' => '',
		'after'  => ''
	] );

	$html = sprintf(
		'<time class="%s" datetime="%s">%s</time>',
		esc_attr( $args['class'] ),
		esc_attr( get_the_date( DATE_W3C ) ),
		sprintf( $args['text'], get_the_date( $args['format'] ) )
	);

	return apply_filters(
		'oak/post/date',
		$args['before'] . $html . $args['after']
	);
}

/**
 * Function which displays your post date in time ago format 
 */
function display_time_ago() {
	echo render_time_ago();
}

/**
 * Function which gets your post date in time ago format 
 * 
 * @return void
 */
function render_time_ago() {
	return human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ).' '.__( 'ago', 'oak-framework' );
}

/**
 * Outputs the post's author avatar.
 * 
 * @access public
 * @param  array   $args
 * @return void
 */
function display_avatar( array $args = [] ) {
	echo render_avatar( $args );
}

/**
 * Returns the post's author avatar
 * 
 * @access public
 * @param  array   $args
 * @return string
 */
function render_avatar( array $args = [] ) {

	$id = get_the_author_meta( 'ID' );

	$args = wp_parse_args( $args, [
		'size' 	  => 96,
		'default' => '',
		'alt'	  => "avatar-img-{$id}",
		'args' 	  => [
			'class' =>  'u-txt-hide',
		],
		'class'  => 'l-entry__avatar',
		'before' => '',
		'after'  => ''
	] );

	$avatar = get_avatar( 
		$id, $args['size'], $args['default'], $args['alt'], $args['args'] 
	);

	$html = sprintf(
		'<div class="%s">%s</div>',
		esc_attr( $args['class'] ),
		$avatar
	);

	return apply_filters(
		'oak/post/avatar',
		$args['before'] . $html . $args['after']
	);
}


/**
 * Outputs the post terms HTML.
 *
 * @access public
 * @param  array  $args
 * @return void
 */
function display_terms( array $args = [] ) {

	echo render_terms( $args );
}

/**
 * Returns the post terms HTML.
 *
 * @access public
 * @param  array  $args
 * @return string
 */
function render_terms( array $args = [] ) {

	$html = '';

	$args = wp_parse_args( $args, [
		'taxonomy' => 'category',
		'text'     => '%s',
		'class'    => '',
		// Translators: Separates tags, categories, etc. when displaying a post.
		'sep'      => _x( ', ', 'taxonomy terms separator', 'oak-framework' ),
		'before'   => '',
		'after'    => ''
	] );

	// Append taxonomy to class name.
	if ( ! $args['class'] ) {
		$args['class'] = "l-entry__terms l-entry__terms--{$args['taxonomy']}";
	}

	$terms = get_the_term_list( get_the_ID(), $args['taxonomy'], '', $args['sep'], '' );

	if ( $terms ) {

		$html = sprintf(
			'<span class="%s">%s</span>',
			esc_attr( $args['class'] ),
			sprintf( $args['text'], $terms )
		);

		$html = $args['before'] . $html . $args['after'];
	}

	return apply_filters( 'oak/post/terms', $html );
}

/**
 * Outputs the post comments link HTML.
 *
 * @access public
 * @param  array  $args
 * @return void
 */
function display_comments_link( array $args = [] ) {

	echo render_comments_link( $args );
}

/**
 * Returns the post comments link HTML.
 *
 * @access public
 * @param  array  $args
 * @return string
 */
function render_comments_link( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'zero'   => false,
		'one'    => false,
		'more'   => false,
		'class'  => 'l-entry__comments',
		'before' => '',
		'after'  => ''
	] );

	$number = get_comments_number();

	if ( 0 == $number && ! comments_open() && ! pings_open() ) {
		return '';
	}

	$url  = get_comments_link();
	$text = get_comments_number( $args['zero'], $args['one'], $args['more'] );

	$html = sprintf(
		'<a class="%s" href="%s">%s</a>',
		esc_attr( $args['class'] ),
		esc_url( $url ),
		$text
	);

	return apply_filters(
		'oak/post/comments',
		$args['before'] . $html . $args['after']
	);
}

/**
 * Outputs the post excerpts.
 * 
 * @access public
 * @param  array 		$args
 * @return void
 */
function display_excerpt( array $args = [] ) {
	echo render_excerpt( $args );
}

/**
 * Returns the post excerpts.
 * 
 * @access public
 * @param  array 		$args
 * @return string
 */
function render_excerpt( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'post'   => null,
		'class'  => 'l-entry__content l-entry__content--excerpt',
	] );

	return sprintf( '<div class="%s">%s</div>', $args['class'], get_the_excerpt( $args['post'] ) );
}

/**
 * Outputs the post content.
 * 
 * @access public
 * @param  array 	$args
 * @return void
 */
function display_content( array $args = [] ) {
	echo render_content( $args );
}

/**
 * Returns the post content.
 * 
 * @access public
 * @param  array 	$args
 * @return string
 */
function render_content( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'post_id' 			=> null,
		'more_link_text' 	=> null,
		'strip_teaser' 		=> false,
		'class'  			=> 'l-entry__content',
		'wrap' 				=> '<div %1$s>%2$s</div>',
	] );

	$class = ( $args['class'] !== '' ) ? 'class="'.esc_attr( $args['class'] ).'"' : '';
	$content      = get_the_content( $args['more_link_text'], $args['strip_teaser'], $args['post_id'] );
	$content 	  = apply_filters( 'the_content', $content );
    $content 	  = str_replace( ']]>', ']]&gt;', $content );

	return sprintf( $args['wrap'], $class, $content ); 
}

/**
 * Outputs the post's featured image.
 * 
 * @access public
 * @param  array 	$args
 * @return void
 */
function display_image( array $args = [] ) { 
	echo render_image( $args );
}

/**
 * Returns the post's featured image
 * 
 * @access public
 * @param  array 	$args
 * @return string
 */
function render_image( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'id' 		 		=> null,
		'default' 	 		=> '',
		'default_class' 	=> 'l-entry__featured',
		'size' 		 		=> '',
		'attr' 		 		=> [],
		'link' 		 		=> ! is_singular(),
		'link_class' 		=> 'l-entry__featured__link',
		'class'  	 		=> 'l-entry__featured',
		'wrap' 		 		=> '<div %1$s>%2$s</div>',
		'before' 	 		=> '',
		'after'  	 		=> ''
	] );

	$args['attr'] = wp_parse_args( $args['attr'], [
		'class' => 'l-entry__featured__image',
	] );

	$image 	= get_the_post_thumbnail( $args['id'], $args['size'], $args['attr'] );

	if ( ! $image && $args['default'] === '' || is_attachment() )
		return;

	if ( ! $image ) {
		$image = "<img class='{$args['attr']['class']} {$args['default_class']}--default' alt='No Image' src='{$args['default']}' />";
	}
	$class = ( $args['class'] !== '' ) ? 'class="'.esc_attr( $args['class'] ).'"' : '';

	if ( $args['link'] ) {
		$image 	= render_permalink( [ 'post_id' => $args['id'], 'class' => $args['link_class'], 'text' => $image ] );
	}

	$html = sprintf( $args['wrap'], $class, $image );

	return apply_filters( 'oak/post/image', $args['before'] . $html . $args['after'] );
}

/**
 * Displays the navigation to next/previous post, when applicable.
 * 
 * @param array 	$args
 * @return void
 */
function navigation_display( $args = array() ) {
   echo navigation_render( $args );
}

/**
 * Retrieves the navigation to next/previous post, when applicable. 
 * This is a fork of the core WordPress `get_the_post_navigation()` function to give
 * theme authors full control over the output of their navigation to next/previous post.
 * 
 * @param array 	$args
 * @return string 	Markup for post links.
 */
function navigation_render( array $args = [] ) {

	$navigation = '';

    $args = wp_parse_args( $args, [
		'prev_text'          	=> '%title',
		'next_text'          	=> '%title',
		'in_same_term'       	=> false,
		'excluded_terms'     	=> '',
		'taxonomy'           	=> 'category',
		'title_text' 			=> __( 'Post navigation' ),
		'wrap_class' 			=> 'c-post-nav',
		'title_class' 			=> 'c-post-nav__title',
		'wrap_inner_class' 		=> 'c-post-nav__inner',
		'prev_link_class' 		=> 'c-post-nav__link c-post-nav__link--prev',
		'next_link_class' 		=> 'c-post-nav__link c-post-nav__link--next',
	] );
 
    $previous = get_previous_post_link(
        "<div class='{$args['prev_link_class']}'>%link</div>",
        $args['prev_text'],
        $args['in_same_term'],
        $args['excluded_terms'],
        $args['taxonomy']
    );
 
    $next = get_next_post_link(
        "<div class='{$args['next_link_class']}'>%link</div>",
        $args['next_text'],
        $args['in_same_term'],
        $args['excluded_terms'],
        $args['taxonomy']
    );
 
    // Only add markup if there's somewhere to navigate to.
    if ( $previous || $next ) {
		$template = '
		<nav class="%1$s" role="navigation">
			<h2 class="%2$s screen-reader-text">%3$s</h2>
			<div class="%4$s">%5$s</div>
		</nav>';
		$navigation = sprintf( $template, sanitize_html_class( $args['wrap_class'] ), $args['title_class'], $args['title_text'], $args['wrap_inner_class'], $previous . $next );
    }
 
    return $navigation;
}

/**
 * Splits the post mime type into two distinct parts: type / subtype
 * (e.g., image / png). Returns an array of the parts.
 *
 * @access public
 * @param  \WP_Post|int  $post  A post object or ID.
 * @return array
 */
function get_mime_types( $post = null ) {

	$type    = get_post_mime_type( $post );
	$subtype = '';

	if ( false !== strpos( $type, '/' ) ) {
		list( $type, $subtype ) = explode( '/', $type );
	}

	return [
		'type'    => $type,
		'subtype' => $subtype
	];
}