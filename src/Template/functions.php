<?php
/**
 * Template functions.
 *
 * Helper functions and template tags related to templates.
 *
 * @package   Oak
 */

namespace Oak\Template;

/**
 * Returns the global hierarchy. This is a wrapper around the values stored via
 * the template hierarchy object.
 *
 * @access public
 * @return array
 */
function hierarchy() {

	return apply_filters(
		'oak/template/hierarchy',
		Hierarchy::hierarchy()
	);
}

/**
 * A better `locate_template()` function than what core WP provides. Note that
 * this function merely locates templates and does no loading. Use the core
 * `load_template()` function for actually loading the template.
 *
 * @access public
 * @param  array|string  $templates
 * @return string
 */
function locate( $templates ) {
	$located = '';

	foreach ( (array) $templates as $template ) {

		foreach ( locations() as $location ) {

			$file = trailingslashit( $location ) . $template;

			if ( file_exists( $file ) ) {
				$located = $file;
				break 2;
			}
		}
	}

	return $located;
}

/**
 * Returns the relative path to where templates are held in the theme.
 *
 * @access public
 * @param  string  $file
 * @return string
 */
function path( $file = '' ) {

	$file = ltrim( $file, '/' );
	$path = apply_filters( 'oak/template/path', 'theme' );

	return $file ? trailingslashit( $path ) . $file : $path;
}

/**
 * Returns an array of locations to look for templates.
 *
 * Note that this won't work with the core WP template hierarchy due to an
 * issue that hasn't been addressed since 2010.
 *
 * @link   https://core.trac.wordpress.org/ticket/13239
 * @access public
 * @return array
 */
function locations() {

	$path = ltrim( path(), '/' );

	// Add active theme path.
	$locations = [ get_stylesheet_directory() . "/{$path}" ];

	// If child theme, add parent theme path second.
	if ( is_child_theme() ) {
		$locations[] = get_template_directory() . "/{$path}";
	}

	return (array) apply_filters( 'oak/template/locations', $locations );
}

/**
 * Filters an array of templates and prefixes them with the view path.
 *
 * @access public
 * @param  array  $templates
 * @return array
 */
function filter_templates( $templates ) {

	$path = path();

	if ( $path ) {
		array_walk( $templates, function( &$template, $key ) use ( $path ) {

			$template = ltrim( str_replace( $path, '', $template ), '/' );

			$template = "{$path}/{$template}";
		} );
	}

	return $templates;
}