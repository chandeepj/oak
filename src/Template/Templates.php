<?php
/**
 * Templates manager.
 *
 * This class is just a wrapper around the `Collection` class for adding a
 * specific type of data.  Essentially, we make sure that anything added to the
 * collection is in fact a `Template`.
 *
 * @package   Oak
 */

namespace Oak\Template;

use Oak\Tools\Collection;

/**
 * Template collection class.
 *
 * @access public
 */
class Templates extends Collection {

	/**
	 * Add a new template.
	 *
	 * @access public
	 * @param  string  $name
	 * @param  mixed   $value
	 * @return void
	 */
	 public function add( $name, $value ) {

		parent::add( $name, new Template( $name, $value ) );
	}
}
