<?php
/**
 * Template functions related to entry comments. The functions 
 * in this file are for handling template tags or features of template 
 * tags that WordPress core does not currently handle.
 * 
 * @package Oak
 */

namespace Oak\Comment;

/**
 * Returns a hierarchy for the current comment.
 *
 * @access public
 * @return array
 */
function hierarchy() {

	$hier = [];
	$type = get_comment_type() ?: 'comment';

	$hier[] = $type;

	if ( in_array( $type, [ 'pingback', 'trackback'] ) ) {

		$hier[] = 'ping';
	}

	return apply_filters( 'oak/comment/hierarchy', $hier );
}

/**
 * Returns the post reactions separated by type.
 *
 * We cannot use the second parameter from comments_template()
 * to separate the reactions because if they are 
 * broken on multiple pages, the count() would only return 
 * the number of comments and pings which are displayed 
 * on the current page, not the total. 
 * 
 * Because of that, we use our own function to get the comments by type.
 * 
 * @access public
 * @return array Post reactions separated by type.
 */
function get_by_type() {
	$comment_args = [
		'order'   => 'ASC',
		'orderby' => 'comment_date_gmt',
		'status'  => 'approve',
		'post_id' => get_the_ID(),
	];

	$comments         = get_comments( $comment_args );
	$comments_by_type = separate_comments( $comments );

	return $comments_by_type;
}

/**
 * Display the language string for the number of comments the current post has.
 *
 * @access public
 * @param array    $args['type'] Optional. Type of comments ( comment | pings ). Default null.
 *                 $args['zero'] Optional. Text for no comments. Default false.
 *                 $args['one']  Optional. Text for one comment. Default false.
 *                 $args['more'] Optional. Text for more than one comment. Default false.
 * @return void
 */
function display_number( $args = [] ) {
	echo render_number( $args );
}

/**
 * Display the language string for the number of comments the current post has.
 *
 * @access public
 * @param array    $args['type'] Optional. Type of comments ( comment | pings ). Default null.
 *                 $args['zero'] Optional. Text for no comments. Default false.
 *                 $args['one']  Optional. Text for one comment. Default false.
 *                 $args['more'] Optional. Text for more than one comment. Default false.
 * @return string
 */
function render_number( $args = [] ) {

    $defaults = [ 
        'type' => '', 
        'zero' => false, 
        'one'  => false, 
        'more' => false
	];
    $args = wp_parse_args( $args, $defaults );

	$comments_by_type = get_by_type();
    if ( $args['type'] === 'comment' ) {

        $response_count = count( $comments_by_type['comment'] ); 
        /* translators: %s: number of comments */
        $response_text = _n( '%s Comment', '%s Comments', $response_count, 'oak-framework' );
        $no_response = __( 'No Comment', 'oak-framework' );
        $one_response = __( '1 Comment', 'oak-framework' );

    } elseif ( $args['type'] === 'pings' ) { 

        $response_count = count( $comments_by_type['pings'] );
        /* translators: %s: number of pings */
        $response_text = _n( '%s Ping', '%s Pings', $response_count, 'oak-framework' );
        $no_response = __( 'No Ping', 'oak-framework' );
        $one_response = __( '1 Ping', 'oak-framework' );
        
    } else {
        $response_count = get_comments_number();
        /* translators: %s: number of responses */
        $response_text = _n( '%s Response', '%s Responses', $response_count, 'oak-framework' );
        $no_response = __( 'No Response', 'oak-framework' );
        $one_response = __( '1 Response', 'oak-framework' );
    }

    if ( $response_count > 1 ) {
        if ( false === $args['more'] ) {
            /* translators: %s: number of comments */
            $output = sprintf( $response_text, number_format_i18n( $response_count ) );
        } else {
            // % Comments
            /* translators: If comment number in your language requires declension,
             * translate this to 'on'. Do not translate into your own language.
             */
            if ( 'on' === _x( 'off', 'Comment number declension: on or off', 'oak-framework' ) ) {
                $text = preg_replace( '#<span class="screen-reader-text">.+?</span>#', '', $args['more'] );
                $text = preg_replace( '/&.+?;/', '', $text ); // Kill entities
                $text = trim( strip_tags( $text ), '% ' );
 
                // Replace '% Comments' with a proper plural form
                if ( $text && ! preg_match( '/[0-9]+/', $text ) && false !== strpos( $args['more'], '%' ) ) {
                    /* translators: %s: number of comments */
                    $new_text = $response_text;
                    $new_text = trim( sprintf( $new_text, '' ) );
 
                    $args['more'] = str_replace( $text, $new_text, $args['more'] );
                    if ( false === strpos( $args['more'], '%' ) ) {
                        $args['more'] = '% ' . $args['more'];
                    }
                }
            }
 
            $output = str_replace( '%', number_format_i18n( $response_count ), $args['more'] );
        }
    } elseif ( $response_count === 0 ) {
        $output = ( false === $args['zero'] ) ? $no_response : $args['zero'];
    } else { // must be one
        $output = ( false === $args['one'] ) ? $one_response : $args['one'];
    }
    /**
     * Filters the comments count for display.
     *
     *
     * @param string $output A translatable string formatted based on whether the count
     *                       is equal to 0, 1, or 1+.
     * @param int    $response_count The number of post response.
     */
    return apply_filters( 'oak/comments/number', $output, $response_count );
}

/**
 * Prints the comment title.
 * 
 * @access public
 * @param  array    (Optional) $args 
 * @return void
 */
function display_title( $args = [] ) {
	echo render_title( $args );
}

/**
 * Retrieve the comment title. Warp comment title with h4.comments-title
 * 
 * @access public
 * @param  array    (Optional) $args 
 * @return string
 */
function render_title( $args = [] ) {

    $defaults = [ 
        'type' => '', 
        'zero' => false, 
        'one'  => false, 
		'more' => false,
		'class' => 'l-comments__title',
        'wrap' => '<h4 %s>%s</h4>'
	];
    $args = wp_parse_args( $args, $defaults );

    $comment_title = render_number( $args );
    $class = "class='{$args['class']}'";
    return sprintf( $args['wrap'], $class, $comment_title );
}

/**
 * Outputs the comment author HTML.
 *
 * @access public
 * @param  array   $args
 * @return void
 */
function display_author( array $args = [] ) {

	echo render_author( $args );
}

/**
 * Returns the comment author HTML.
 *
 * @access public
 * @param  array   $args
 * @return string
 */
function render_author( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'text'   => '%s',
		'class'  => 'c-comment__author',
		'before' => '',
		'after'  => ''
	] );

	$html = sprintf(
		'<span class="%s">%s</span>',
		esc_attr( $args['class'] ),
		get_comment_author_link()
	);

	return apply_filters( 'oak/comment/author', $args['before'] . $html . $args['after'] );
}

/**
 * Outputs the comment permalink HTML.
 *
 * @access public
 * @param  array   $args
 * @return void
 */
function display_permalink( array $args = [] ) {

	echo render_permalink( $args );
}

/**
 * Returns the comment permalink HTML.
 *
 * @access public
 * @param  array   $args
 * @return string
 */
function render_permalink( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'text'   => '%s',
		'class'  => 'c-comment__permalink',
		'before' => '',
		'after'  => ''
	] );

	$url = get_comment_link();

	$html = sprintf(
		'<a class="%s" href="%s">%s</a>',
		esc_attr( $args['class'] ),
		esc_url( $url ),
		sprintf( $args['text'], esc_url( $url ) )
	);

	return apply_filters( 'oak/comment/permalink', $args['before'] . $html . $args['after'] );
}

/**
 * Outputs the comment date HTML.
 *
 * @access public
 * @param  array   $args
 * @return void
 */
function display_date( array $args = [] ) {

	echo render_date( $args );
}

/**
 * Returns the comment date HTML.
 *
 * @access public
 * @param  array   $args
 * @return string
 */
function render_date( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'text'   => '%s',
		'format' => '',
		'class'  => 'c-comment__date',
		'before' => '',
		'after'  => ''
	] );

	$url = get_comment_link();

	$html = sprintf(
		'<time class="%s" datetime="%s">%s</time>',
		esc_attr( $args['class'] ),
		esc_attr( get_comment_date( DATE_W3C ) ),
		sprintf( $args['text'], esc_html( get_comment_date( $args['format'] ) ) )
	);

	return apply_filters( 'oak/comment/date', $args['before'] . $html . $args['after'] );
}

/**
 * Outputs the comment time HTML.
 *
 * @access public
 * @param  array   $args
 * @return void
 */
function display_time( array $args = [] ) {

	echo render_time( $args );
}

/**
 * Returns the comment time HTML.
 *
 * @access public
 * @param  array   $args
 * @return string
 */
function render_time( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'text'   => '%s',
		'format' => '',
		'class'  => 'c-comment__time',
		'before' => '',
		'after'  => ''
	] );

	$url = get_comment_link();

	$html = sprintf(
		'<time class="%s" datetime="%s">%s</time>',
		esc_attr( $args['class'] ),
		esc_attr( get_comment_date( DATE_W3C ) ),
		sprintf( $args['text'], esc_html( get_comment_time( $args['format'] ) ) )
	);

	return apply_filters( 'oak/comment/time', $args['before'] . $html . $args['after'] );
}

/**
 * Outputs the comment edit link HTML.
 *
 * @access public
 * @param  array   $args
 * @return void
 */
function display_edit_link( array $args = [] ) {

	echo render_edit_link( $args );
}

/**
 * Returns the comment edit link HTML.
 *
 * @access public
 * @param  array   $args
 * @return string
 */
function render_edit_link( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'text'   => __( 'Edit', 'oak-framework' ),
		'class'  => 'c-comment__edit',
		'before' => '',
		'after'  => ''
	] );

	$html = '';
	$url  = get_edit_comment_link();

	if ( $url ) {

		$html = sprintf(
			'<a class="%s" href="%s">%s</a>',
			esc_attr( $args['class'] ),
			esc_url( $url ),
			$args['text']
		);

		$html = $args['before'] . $html . $args['after'];
	}

	return apply_filters( 'oak/comment/edit/link', $html );
}

/**
 * Outputs the comment reply link HTML.
 *
 * @access public
 * @param  array   $args
 * @return void
 */
function display_reply_link( array $args = [] ) {

	echo render_reply_link( $args );
}

/**
 * Returns the comment reply link HTML.  Note that WP's `comment_reply_link()`
 * doesn't work outside of `wp_list_comments()` without passing in the proper
 * arguments (it isn't meant to).  This function is just a wrapper for
 * `get_comment_reply_link()`, which adds in the arguments automatically.
 *
 * @access public
 * @param  array  $args
 * @return string
 */
function render_reply_link( array $args = [] ) {

	$args = wp_parse_args( $args, [
		'before'    => '',
		'after'     => '',
		'depth'     => intval( $GLOBALS['comment_depth'] ),
		'max_depth' => get_option( 'thread_comments_depth' ),
		'class'     => 'c-comment__reply',
		'post_id'   => get_the_ID()
	] );

	// Array of comment types that are not allowed to have replies.
	$disallowed = [
		'pingback',
		'trackback'
	];

	$before = $args['before'];
	$after  = $args['after'];

	unset( $args['before'], $args['after'] );

	$link = get_comment_reply_link( $args );

	if ( ! get_option( 'thread_comments' ) || in_array( get_comment_type(), $disallowed ) || ! $link ) {
		return '';
	}

	$html = preg_replace(
		"/class=(['\"]).+?(['\"])/i",
		'class=$1' . esc_attr( $args['class'] ) . ' comment-reply-link$2',
		$link,
		1
	);

	return apply_filters( 'oak/comment/reply/link', $before . $html . $after );
}

/**
 * Conditional function to check if a comment is approved.
 *
 * @access public
 * @param  WP_Comment|int  Comment object or ID.
 * @return bool
 */
function is_approved( $comment = null ) {
	$comment = get_comment( $comment );

	return 'approved' === wp_get_comment_status( $comment->ID );
}

/**
 * Conditional function to check if a comment is closed.
 * 
 * @access public
 * @return bool
 */
function is_closed( $post_id = null  ) {
    return ( ( ! comments_open( $post_id ) ) && have_comments() );
}
