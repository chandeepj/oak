<?php
/**
 * Pagination functions.
 *
 * Helper functions and template tags related to pagination.
 *
 * @package   Oak
 */

namespace Oak\Pagination;

/**
 * Outputs the pagination output.
 *
 * @access public
 * @param  string $context
 * @param  array  $args
 * @return object
 */
function display( $context = 'posts', array $args = [] ) {

	( new Pagination( $context, $args ) )->make()->display();
}

/**
 * Returns the pagination output.
 *
 * @access public
 * @param  string $context
 * @param  array  $args
 * @return object
 */
function render( $context = 'posts', array $args = [] ) {

	return ( new Pagination( $context, $args ) )->make()->render();
}