<?php
/**
 * Helper functions for working with the WordPress sidebar system.  Currently, the framework creates a
 * simple function for registering HTML5-ready sidebars instead of the default WordPress unordered lists.
 *
 * @package Oak
 */

namespace Oak\Sidebar;

/**
 * Wrapper function for WordPress' register_sidebar() function.  This function exists so that theme authors
 * can more quickly register sidebars with an HTML5 structure instead of having to write the same code
 * over and over.  Theme authors are also expected to pass in the ID, name, and description of the sidebar.
 * This function can handle the rest at that point.
 * 
 * @access public
 * @param  array   $args
 * @return string  Sidebar ID.
 */
function register( $args ) {

	// Set up some default sidebar arguments.
	$defaults = [
		'id'            => '',
		'name'          => '',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title widget--title">',
		'after_title'   => '</h3>'
	];

	// Parse the arguments.
	$args = wp_parse_args( $args, apply_filters( 'oak/sidebar/defaults', $defaults ) );

	// Register the sidebar.
	return register_sidebar( apply_filters( 'oak/sidebar/args', $args ) );
}

/**
 * Function for grabbing a dynamic sidebar name.
 *
 * @access public
 * @global array   $wp_registered_sidebars
 * @param  string  $sidebar_id
 * @return string
 */
function get_name( $sidebar_id ) {
	global $wp_registered_sidebars;

	return isset( $wp_registered_sidebars[ $sidebar_id ] ) ? $wp_registered_sidebars[ $sidebar_id ]['name'] : '';
}

/**
 * Checks if a widget exists.  Pass in the widget class name.  This function is useful for
 * checking if the widget exists before directly calling `the_widget()` within a template.
 * 
 * @access public
 * @param  string  $widget
 * @return bool
 */
function widget_exists( $widget ) {

	return isset( $GLOBALS['wp_widget_factory']->widgets[ $widget ] );
}

/**
 * Adds Placeholder widget
 * 
 * @access public
 * @param  array  $args
 * @return bool
 */
function placeholder_widget( $args = array() ) {
	// Set up some default sidebar arguments.
	$defaults = array(
		'before_widget' => '<section class="widget widget_text">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>'
	);

	// Parse the arguments.
	$args = wp_parse_args( $args, $defaults );

	the_widget(
		'WP_Widget_Text',
		array(
			'title'  => __( 'Example Widget', 'acorn' ),
			// Translators: The %s are placeholders for HTML, so the order can't be changed.
			'text'   => sprintf( __( 'This is an example widget to show how the Primary sidebar looks by default. You can add custom widgets from the %swidgets screen%s in the admin.', 'acorn' ), current_user_can( 'edit_theme_options' ) ? '<a href="' . admin_url( 'widgets.php' ) . '">' : '', current_user_can( 'edit_theme_options' ) ? '</a>' : '' ),
			'filter' => true,
		),
		array(
			'before_widget' => $args['before_widget'],
			'after_widget'  => $args['after_widget'],
			'before_title'  => $args['before_title'],
			'after_title'   => $args['after_title']
		)
	);
}