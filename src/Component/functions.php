<?php
/**
 * Components.
 *
 * Component template tags. like c-post-nav, c-related-post
 * 
 * @package Oak
 */

namespace Oak\Component;

/**
 * Retrieves an array of related posts.
 * 
 * @param  int 		$count   		(optional) No. of Related Posts to show.
 * @param  string 	$post_type 		(optional) Post Type name
 * @param  string 	$taxonomy 		(optional) Taxonomy name
 * @return array 			 		(optional) List of posts    
 */
function related_posts_query( $count = 5, $post_type = 'post', $taxonomy = 'category' ) {

	if( ! is_singular( $post_type ) ) {
		return;
	}

    # Get current course ID
    $id = get_the_ID();

    # Get the categories for the current course.
    $cats = wp_get_object_terms( $id, $taxonomy, [ 'fields' => 'ids' ] );

    $query_args = [
        'post_type'         => $post_type,
        'post_status'       => 'publish',
        'showposts'         => $count,
        'orderby'           => 'rand',
        'tax_query'         => [
            [
                'taxonomy'     => $taxonomy,
                'field'         => 'term_id',
                'terms'        => $cats
            ]
        ],
        'post__not_in'      => [ $id ],
    ];

    $tax_query = new \WP_Query;

    return $tax_query->query($query_args);
}

/**
 * Display custom search form
 *
 * @param string $form Form HTML.
 * @return void
 */
function display_search_form() {
    echo render_search_form();
}

/**
 * Returns custom search form
 *
 * @param string $form Form HTML.
 * @return string Modified form HTML.
 */
function render_search_form() {

    $action = home_url( '/' );
    $label = __( 'Search for:', 'oak-framework' );
    $query = get_search_query();
    $value = esc_attr__( 'Search', 'oak-framework' );

    $form = "
        <form class='c-search c-search--wp u-flex' role='search' method='get' action='{$action}'>
            <div class='c-search__field c-field u-flex-grow'>
                <label>
                    <span class='screen-reader-text'>{$label}</span>
                    <input class='c-field__input u-w-full' type='text' placeholder='Search...' value='{$query}' name='s' />
                </label>
            </div>
            <input class='c-btn c-btn--primary u-ml-1 u-w-16' type='submit' value='{$value}' />
        </form>";

    return $form;
}