<?php
/**
 * Utility functions which helps with the theme functionality.
 *
 * @package Oak
 */

namespace Oak;

/**
 * Replaces `%1$s` & `%2$s` with the template & stylesheet directory URIs.
 *
 * @param  string  $value
 * @return string
 */
function sprintf_theme_uri( $value ) {
	return sprintf( $value, get_template_directory_uri(), get_stylesheet_directory_uri() );
}

/**
 * Gets Dir paths
 * 
 * @param  string 	$dir
 * @return string
 */
function theme_dir( $dir = '' ) {

	switch ( $dir ) {
		case 'child': # Child theme dir
			$dir_path = trailingslashit( get_theme_file_path() );
			break;
		default: # By default, returns Parent theme dir
			$dir_path = trailingslashit( get_parent_theme_file_path() );
			break;
	}
	return $dir_path;
}

/**
 * Gets Dir URIs
 * 
 * @param  string 	$uri
 * @return string
 */
function theme_uri( $uri = '' ) {

	switch ( $uri ) {
		case 'child': # Child theme uri
			$dir_uri = trailingslashit( get_theme_file_uri() );
			break;
		default: # By default, returns Parent theme uri
			$dir_uri = trailingslashit( get_parent_theme_file_uri() );
			break;
	}
	return $dir_uri;
}

/**
 * Works same like `in_array()` but check for multiple values.
 * 
 * It checks if ALL `$needles` exist
 * 
 * `Oak\in_array_any($needles, $haystack)` is equal 
 * 		to `(in_array('foo',$arg) && in_array('bar',$arg))`
 * 
 * @param  array 	$needles
 * @param  array 	$haystack
 * @return bool
 */
function in_array_all( $needles, $haystack ) {
	return empty( array_diff( $needles, $haystack ) );
}

/**
 * Works same like `in_array()` but check for multiple values.
 * 
 * It checks if ANY `$needles` exist
 * 
 * `Oak\in_array_any($needles, $haystack)` is equal 
 * 		to `(in_array('foo',$arg) || in_array('bar',$arg))`
 * 
 * @param  array 	$needles
 * @param  array 	$haystack
 * @return bool
 */
function in_array_any( $needles, $haystack ) {
	return (bool) array_intersect( $needles, $haystack );
}

/**
 * Helper function for replacing a class in an HTML string. This function only
 * replaces the first class attribute it comes upon and stops.
 *
 * @access public
 * @param  string  $class
 * @param  string  $html
 * @return string
 */
function replace_html_class( $class, $html ) {

	return preg_replace(
		"/class=(['\"]).+?(['\"])/i",
		'class=$1' . esc_attr( $class ) . '$2',
		$html,
		1
	);
}