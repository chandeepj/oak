<?php
/**
 * Bootstrap Functions
 *
 * @package   Oak
 */

require_once( 'functions-helpers.php' );
require_once( 'functions-filters.php' );

array_map( function( $Folder ) {
	require dirname( __FILE__ ) . "/{$Folder}/functions.php";
}, [
    'Template',
    'View',
	'Attr',
	'Site',
	'Pagination',
	'Post',
	'Sidebar',
	'Comment',
	'Component'
] );