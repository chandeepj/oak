<?php
/**
 * Attribute functions.
 *
 * HTML attribute functions and filters.  The purposes of this is to provide a
 * way for theme/plugin devs to hook into the attributes for specific HTML
 * elements and create new or modify existing attributes. This is sort of like
 * `body_class()`, `post_class()`, and `comment_class()` on steroids.  Plus, it
 * handles attributes for many more elements.  The biggest benefit of using this
 * is to provide richer microdata while being forward compatible with the
 * ever-changing Web.
 *
 * @package Oak
 */

namespace Oak\Attr;

/**
 * Wrapper for creating a new `Attributes` object.
 *
 * @access public
 * @param  string  $name
 * @param  string  $context
 * @param  array   $attr
 * @return Attributes
 */
function attr( $name, $context = '', array $attr = [] ) {

	return new Attr( $name, $context, $attr );
}

/**
 * Outputs an HTML element's attributes.
 *
 * @access public
 * @param  string  $slug
 * @param  string  $context
 * @param  array   $attr
 * @return void
 */
function display( $slug, $context = '', $attr = [] ) {

	attr( $slug, $context, $attr )->display();
}

/**
 * Returns an HTML element's attributes.
 *
 * @access public
 * @param  string  $slug
 * @param  string  $context
 * @param  array   $attr
 * @return string
 */
function render( $slug, $context = '', $attr = [] ) {

	return attr( $slug, $context, $attr )->render();
}
